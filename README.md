# Francesc Marco Spt Data Analyst Test

Hello! This ReadMe file contains just the conclusions of the data analysis. If you want to check the code, there is a Python Jupyter Notebook file attached in the repository. If you want to check the SQL querys, there is a .word file attached too with some anotations. Let's start!

## Conclusions

### Global and Curious insights:
- The vast majority of users register the same day of installation. The rest of users register during the next 100 days approximately.
- Most of the users have an end_level between 0 and 500.
- Users have approximately between 0 and 15 number of sessions being 0 the most common.
- Users usually play between 0 and 30.000 seconds (8 hours).

## About the questions:

- Our biggest market is in Android Devices. (Revenue from Android > Revenue from IOS)
- Our best revenue source comes from Adds (Revenue from Adds > Revenue from In app purchases)
- Users from IOS hardly make in-app purchases.
- Our biggest geography market is in US by far followed by GB, Australia and California
- The Android campaign is more expensive than the IOS as it is normal because of the size of the market.
- Looks like IOS and ANDROID had similar valid installations (20655 vs 24780)
- Apparently, the IOS campaign had a better ROI than the Android. That was quite interesting, I thought that would be the other way around.

Thats almost all the information I could extract. I hope you like it!